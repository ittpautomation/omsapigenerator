echo cd "`dirname "$0"`"
cd "`dirname "$0"`"
echo git checkout master
git checkout master
echo git branch -d $(date '+%Y%m%d')_$(git config --list | grep name | cut -d'=' -f 2 | cut -d' ' -f 1)
git branch -d $(date '+%Y%m%d')_$(git config --list | grep name | cut -d'=' -f 2 | cut -d' ' -f 1)
echo git checkout -b $(date '+%Y%m%d')_$(git config --list | grep name | cut -d'=' -f 2 | cut -d' ' -f 1)
git checkout -b $(date '+%Y%m%d')_$(git config --list | grep name | cut -d'=' -f 2 | cut -d' ' -f 1)
echo git checkout $(date '+%Y%m%d')_$(git config --list | grep name | cut -d'=' -f 2 | cut -d' ' -f 1)
git checkout $(date '+%Y%m%d')_$(git config --list | grep name | cut -d'=' -f 2 | cut -d' ' -f 1)
echo git add --all
git add --all
echo git commit -m 'created branch for EOB'
git commit -m 'created branch for EOB'
echo git push origin $(date '+%Y%m%d')_$(git config --list | grep name | cut -d'=' -f 2 | cut -d' ' -f 1)
git push origin $(date '+%Y%m%d')_$(git config --list | grep name | cut -d'=' -f 2 | cut -d' ' -f 1)