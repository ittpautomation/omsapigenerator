import org.json.JSONObject;

import javax.swing.*;
import java.awt.*;
import java.util.List;

class MyAuthPanel {
    final JTextField txtMachineToken;
    final JTextField txtUserTokenUser;
    private final JTextField txtUserTokenPW;
    final JTextField txtBasketID;
    final JTextField txtPaymentToken;
    final JTextField txtUserToken;
    final JTextField txtUserID;
    final JButton btnGetMachinetoken;
    final JButton btnGetUserToken;
    final JButton btnGetBasketID;
    final JButton btnGetPaymentToken;

    MyAuthPanel(JPanel p_Allgemein, MyHeaderPanel h, JsonBuilderOMS jsbo){
        JPanel p_Auth = new JPanel();
        p_Auth.setMinimumSize(new Dimension(10, 120));
        p_Auth.setMaximumSize(new Dimension(32767, 120));
        p_Allgemein.add(p_Auth);
        p_Auth.setLayout(new GridLayout(0, 1, 0, 0));

        JTabbedPane tp_Auth = new JTabbedPane(JTabbedPane.TOP);
        tp_Auth.setMaximumSize(new Dimension(32767, 130));
        tp_Auth.setPreferredSize(new Dimension(400, 130));
        p_Auth.add(tp_Auth);

        JPanel p_MachineToken = new JPanel();
        tp_Auth.addTab("Machine Token", null, p_MachineToken, null);
        p_MachineToken.setLayout(new GridLayout(0, 1, 0, 0));

        btnGetMachinetoken = new JButton("Get MachineToken");
        p_MachineToken.add(btnGetMachinetoken);

        JPanel p_machineToken = new JPanel();
        p_MachineToken.add(p_machineToken);
        p_machineToken.setLayout(new BoxLayout(p_machineToken, BoxLayout.X_AXIS));

        JLabel lblMachineToken = new JLabel(" MachineToken");
        lblMachineToken.setPreferredSize(new Dimension(100, 14));
        lblMachineToken.setMinimumSize(new Dimension(100, 14));
        lblMachineToken.setMaximumSize(new Dimension(100, 14));
        p_machineToken.add(lblMachineToken);

        txtMachineToken = new JTextField();
        p_machineToken.add(txtMachineToken);
        txtMachineToken.setColumns(10);

        JPanel p_UserToken = new JPanel();
        tp_Auth.addTab("User Token", null, p_UserToken, null);
        p_UserToken.setLayout(new GridLayout(0, 1, 0, 0));

        txtUserTokenUser = new JTextField();
        p_UserToken.add(txtUserTokenUser);
        txtUserTokenUser.setColumns(10);

        txtUserTokenPW = new JTextField();
        p_UserToken.add(txtUserTokenPW);
        txtUserTokenPW.setColumns(10);

        btnGetUserToken = new JButton("Get User Token");
        p_UserToken.add(btnGetUserToken);
        btnGetUserToken.setMaximumSize(new Dimension(2147483647, 20));

        JPanel p_userToken = new JPanel();
        p_UserToken.add(p_userToken);
        p_userToken.setLayout(new BoxLayout(p_userToken, BoxLayout.X_AXIS));

        JLabel lblUsertoken = new JLabel(" UserToken ");
        lblUsertoken.setMinimumSize(new Dimension(100, 14));
        lblUsertoken.setMaximumSize(new Dimension(100, 14));
        lblUsertoken.setPreferredSize(new Dimension(100, 14));
        p_userToken.add(lblUsertoken);

        txtUserToken = new JTextField();
        p_userToken.add(txtUserToken);
        txtUserToken.setColumns(10);

        JPanel p_userID = new JPanel();
        p_UserToken.add(p_userID);
        p_userID.setLayout(new BoxLayout(p_userID, BoxLayout.X_AXIS));

        JLabel lblUserid = new JLabel(" UserID         ");
        lblUserid.setPreferredSize(new Dimension(100, 14));
        lblUserid.setMaximumSize(new Dimension(100, 100));
        p_userID.add(lblUserid);

        txtUserID = new JTextField();
        p_userID.add(txtUserID);
        txtUserID.setColumns(10);

        JPanel p_BasketID = new JPanel();
        tp_Auth.addTab("BasketID", null, p_BasketID, null);
        p_BasketID.setLayout(new GridLayout(0, 1, 0, 0));

        btnGetBasketID = new JButton("Get Basket ID");
        p_BasketID.add(btnGetBasketID);
        btnGetBasketID.setEnabled(false);

        JPanel p_basketID = new JPanel();
        p_BasketID.add(p_basketID);
        p_basketID.setLayout(new BoxLayout(p_basketID, BoxLayout.X_AXIS));

        JLabel lblBasketid = new JLabel(" BasketID ");
        lblBasketid.setMinimumSize(new Dimension(10, 14));
        lblBasketid.setMaximumSize(new Dimension(100, 14));
        lblBasketid.setPreferredSize(new Dimension(100, 14));
        p_basketID.add(lblBasketid);

        txtBasketID = new JTextField();
        p_basketID.add(txtBasketID);
        txtBasketID.setColumns(10);

        JPanel p_PaymentToken = new JPanel();
        tp_Auth.addTab("Payment Token", null, p_PaymentToken, null);
        p_PaymentToken.setLayout(new GridLayout(0, 1, 1, 0));

        btnGetPaymentToken = new JButton("Get Payment Token");
        p_PaymentToken.add(btnGetPaymentToken);
        btnGetPaymentToken.setEnabled(false);

        JPanel p_paymentToken = new JPanel();
        p_PaymentToken.add(p_paymentToken);
        p_paymentToken.setLayout(new BoxLayout(p_paymentToken, BoxLayout.X_AXIS));

        JLabel lblNewLabel = new JLabel(" Payment Token ");
        lblNewLabel.setPreferredSize(new Dimension(100, 14));
        lblNewLabel.setMaximumSize(new Dimension(100, 14));
        lblNewLabel.setMinimumSize(new Dimension(100, 14));
        p_paymentToken.add(lblNewLabel);

        txtPaymentToken = new JTextField();
        p_paymentToken.add(txtPaymentToken);
        txtPaymentToken.setColumns(10);

        btnGetUserToken.addActionListener(e -> {
            List<String> values = work.uToken(work.getHeaderFromTable(h.t_userToken,txtUserTokenUser.getText(),txtUserID.getText(),txtMachineToken.getText(),txtUserToken.getText()),txtUserTokenUser.getText(), txtUserTokenPW.getText());
            work.putResultToField(values, txtUserToken, txtUserID);
        });

        btnGetBasketID.addActionListener(e -> {
            List<String> values = work.basketID(work.getHeaderFromTable(h.t_basketID,txtUserTokenUser.getText(),txtUserID.getText(),txtMachineToken.getText(),txtUserToken.getText()), txtUserID.getText());
            work.putResultToField(values, txtBasketID);
        });

        btnGetMachinetoken.addActionListener(e -> {
            List<String> values = work.mToken(work.getHeaderFromTable(h.t_machineToken,txtUserTokenUser.getText(),txtUserID.getText(),txtMachineToken.getText(),txtUserToken.getText()));
            work.putResultToField(values, txtMachineToken);
        });

        btnGetPaymentToken.addActionListener(arg0 -> {
            List<String> values = work.paymentToken(work.getHeaderFromTable(h.t_paymentToken,txtUserTokenUser.getText(),txtUserID.getText(),txtMachineToken.getText(),txtUserToken.getText()), txtUserID.getText(), txtUserTokenUser.getText(), jsbo);
            work.putResultToField(values, txtPaymentToken);
        });

        tp_Auth.addChangeListener(ce -> {
            switch(tp_Auth.getSelectedIndex()){
                case 0:
                    //System.out.println("MachineToken");
                    break;
                case 1:
                    //System.out.println("UserToken");
                    break;
                case 2:
                    //System.out.println("BasketID");
                    if(!txtUserID.getText().equals("") && (!txtUserToken.getText().equals("") || !txtMachineToken.getText().equals(""))){
                        btnGetBasketID.setEnabled(true);
                    }else{
                        btnGetBasketID.setEnabled(false);
                    }
                    break;
                case 3:
                    //System.out.println("PaymentToken");
                    System.out.println(txtBasketID.getText());
                    if(!txtUserID.getText().equals("") && (!txtUserToken.getText().equals("") || !txtMachineToken.getText().equals("")) && !txtBasketID.getText().equals("") && !StartFrame.txtResultJson.getText().equals("")){
                        JSONObject jsoBody = new JSONObject(StartFrame.txtResultJson.getText());
                        if(jsoBody.getInt("orderValue")<4000){
                            btnGetPaymentToken.setEnabled(false);
                        }else {
                            btnGetPaymentToken.setEnabled(true);
                        }
                    }else{
                        btnGetPaymentToken.setEnabled(false);
                    }
                    break;
            }
        });

        // DEBUGGING INFORMATION
        txtUserTokenUser.setText("rewe_ffc20-plz50733@yahoo.com");
        txtUserTokenPW.setText("STart.01");
    }
}
