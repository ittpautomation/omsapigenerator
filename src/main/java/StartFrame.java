import java.awt.EventQueue;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.Dimension;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class StartFrame extends JFrame {

    private final JsonBuilderOMS jsbo = new JsonBuilderOMS();
    private final JTextField txt_qty;
    private final JTextField txtTimeSlotTo;
    private final JTextField txtTimeSlotFrom;
    private final JTextField txtEan;
    private final JTextField txtText;
    private final JTextField txtType;
    private final JTextField txtWWident;
    private final List<List<String>> addedItemList;
    private final List<List<String>> couponList;
    private final List<List<String>> fulladdresslist;
    private final List<List<String>> merchLists;
    private final JComboBox<String> cb_Shipping;
    private final JComboBox<String> cb_Invoice;
    private final JComboBox<Object> cb_MerchInfo;
    static JTextArea txt_OrderDetailsBody;
    static JLabel lblMessage;
    static JTextArea txtResultJson;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                StartFrame frame = new StartFrame();
                frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Create the frame.
     */
    private StartFrame() {
        setTitle("OMS API Generator v2");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(100, 100, 700, 700);
        setMinimumSize(new Dimension(700,700));
        //add menu
        MyMenu m = new MyMenu(this);
        JPanel cP = new JPanel();
        cP.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(cP);
        cP.setLayout(new BoxLayout(cP, BoxLayout.PAGE_AXIS));

        JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
        cP.add(tabbedPane_1);
        // Allgemein panel in contentPane-------------------------------------------------------------------------------
        JPanel p_Allgemein = new JPanel();
        tabbedPane_1.addTab("Allgemein", null, p_Allgemein, null);
        p_Allgemein.setLayout(new BoxLayout(p_Allgemein, BoxLayout.PAGE_AXIS));

        //add enpoint panel
        MyEndpointModePanel epm = new MyEndpointModePanel(p_Allgemein);

        //add header panel
        MyHeaderPanel h = new MyHeaderPanel(p_Allgemein);

        SQL sql = new SQL();
        if(!sql.connect()){
            System.out.println("SQL CONNECTION ERROR");
        }

        //get address from mysql database
        fulladdresslist = new ArrayList<>(sql.getAddressList());
        cb_Shipping = new JComboBox<>();
        cb_Invoice = new JComboBox<>();
        for(String l : sql.getDropDownInformation(fulladdresslist)) {
            cb_Shipping.addItem(l);
            cb_Invoice.addItem(l);
        }

        //add auth panel
        MyAuthPanel a = new MyAuthPanel(p_Allgemein, h, jsbo);

        //add repsone panel
        new MyResponsePanel(p_Allgemein);
        
        JPanel p_data = new JPanel();
        tabbedPane_1.addTab("Data", null, p_data, null);
        p_data.setLayout(new BoxLayout(p_data, BoxLayout.PAGE_AXIS));
        // Allgemein panel in contentPane-------------------------------------------------------------------------------
        // Address panel in data tab------------------------------------------------------------------------------------
        JPanel p_Adresse = new JPanel();
        p_data.add(p_Adresse);
        p_Adresse.setPreferredSize(new Dimension(3440, 60));
        p_Adresse.setMinimumSize(new Dimension(10, 60));
        p_Adresse.setMaximumSize(new Dimension(32767, 60));
        p_Adresse.setLayout(new GridLayout(2, 6, 0, 0));

        // Invoice line in address panel--------------------------------------------------------------------------------
        JPanel p_invoice = new JPanel();
        p_invoice.setLayout(new BoxLayout(p_invoice, BoxLayout.X_AXIS));
        p_Adresse.add(p_invoice);

        JLabel lblNewLabel_2 = new JLabel("Invoice Address:    ");
        lblNewLabel_2.setMinimumSize(new Dimension(150, 14));
        lblNewLabel_2.setMaximumSize(new Dimension(150, 14));
        lblNewLabel_2.setPreferredSize(new Dimension(150, 14));
        p_invoice.add(lblNewLabel_2);

        p_invoice.add(cb_Invoice);
        // Invoice line in address panel--------------------------------------------------------------------------------
        // Shipping line in address panel-------------------------------------------------------------------------------
        JPanel p_billing = new JPanel();
        p_billing.setLayout(new BoxLayout(p_billing, BoxLayout.X_AXIS));
        p_Adresse.add(p_billing);
        
        JLabel lblNewLabel_1 = new JLabel("Shipping Address: ");
        lblNewLabel_1.setPreferredSize(new Dimension(150, 14));
        lblNewLabel_1.setMinimumSize(new Dimension(150, 14));
        lblNewLabel_1.setMaximumSize(new Dimension(150, 14));
        p_billing.add(lblNewLabel_1);

        p_billing.add(cb_Shipping);
        // Shipping line in address panel-------------------------------------------------------------------------------

        JPanel p_Items = new JPanel();
        p_Items.setPreferredSize(new Dimension(3440, 75));
        p_Items.setMinimumSize(new Dimension(3440, 75));
        p_Items.setMaximumSize(new Dimension(3440, 75));
        p_data.add(p_Items);
        p_Items.setLayout(new GridLayout(2, 0, 0, 0));
        
        JPanel p_ItemSelect = new JPanel();
        p_Items.add(p_ItemSelect);
        p_ItemSelect.setLayout(new BoxLayout(p_ItemSelect, BoxLayout.X_AXIS));
        
        JLabel lblItems = new JLabel("Items:");
        lblItems.setPreferredSize(new Dimension(150, 14));
        lblItems.setMinimumSize(new Dimension(150, 14));
        lblItems.setMaximumSize(new Dimension(150, 14));
        p_ItemSelect.add(lblItems);
        
        JComboBox<String> cb_Items = new JComboBox<>();
        cb_Items.setMinimumSize(new Dimension(350, 50));
        cb_Items.setMaximumSize(new Dimension(350, 50));
        cb_Items.setPreferredSize(new Dimension(350, 50));
        p_ItemSelect.add(cb_Items);

        txt_qty = new JTextField();
        txt_qty.setText("1");
        txt_qty.setToolTipText("Quantity");
        txt_qty.setPreferredSize(new Dimension(75, 50));
        txt_qty.setMaximumSize(new Dimension(75, 50));
        txt_qty.setMinimumSize(new Dimension(75, 50));
        p_ItemSelect.add(txt_qty);
        txt_qty.setColumns(10);
        
        JButton btnAdd = new JButton("Add");
        btnAdd.setMinimumSize(new Dimension(75, 50));
        btnAdd.setMaximumSize(new Dimension(75, 50));
        btnAdd.setPreferredSize(new Dimension(75, 50));
        p_ItemSelect.add(btnAdd);

        JPanel p_ItemAdded = new JPanel();
        p_Items.add(p_ItemAdded);
        p_ItemAdded.setLayout(new BoxLayout(p_ItemAdded, BoxLayout.X_AXIS));
        
        JLabel lblItemOverview = new JLabel("Item Overview:");
        lblItemOverview.setMaximumSize(new Dimension(150, 14));
        lblItemOverview.setMinimumSize(new Dimension(150, 14));
        lblItemOverview.setPreferredSize(new Dimension(150, 14));
        p_ItemAdded.add(lblItemOverview);
        
        JComboBox<String> cb_AddedItems = new JComboBox<>();
        cb_AddedItems.setMinimumSize(new Dimension(350, 50));
        cb_AddedItems.setMaximumSize(new Dimension(350, 50));
        cb_AddedItems.setPreferredSize(new Dimension(350, 50));
        p_ItemAdded.add(cb_AddedItems);
        
        JButton btnRemove = new JButton("Remove");
        btnRemove.setMinimumSize(new Dimension(100, 50));
        btnRemove.setMaximumSize(new Dimension(100, 50));
        btnRemove.setPreferredSize(new Dimension(100, 50));
        p_ItemAdded.add(btnRemove);
        
        JButton btnModify = new JButton("Modify");
        btnModify.setPreferredSize(new Dimension(75, 50));
        btnModify.setMinimumSize(new Dimension(75, 50));
        btnModify.setMaximumSize(new Dimension(75, 50));
        p_ItemAdded.add(btnModify);
        
        JPanel p_TimeSlot = new JPanel();
        p_TimeSlot.setMinimumSize(new Dimension(3440, 75));
        p_TimeSlot.setMaximumSize(new Dimension(3440, 75));
        p_TimeSlot.setPreferredSize(new Dimension(3440, 75));
        p_data.add(p_TimeSlot);
        p_TimeSlot.setLayout(new GridLayout(3, 0, 0, 0));
        
        JPanel p_TimeSlotFrom = new JPanel();
        p_TimeSlot.add(p_TimeSlotFrom);
        p_TimeSlotFrom.setLayout(new BoxLayout(p_TimeSlotFrom, BoxLayout.X_AXIS));
        
        JLabel lblTimeslotFrom = new JLabel("TimeSlot From:");
        lblTimeslotFrom.setPreferredSize(new Dimension(150, 14));
        lblTimeslotFrom.setMinimumSize(new Dimension(150, 14));
        lblTimeslotFrom.setMaximumSize(new Dimension(150, 14));
        p_TimeSlotFrom.add(lblTimeslotFrom);
        
        txtTimeSlotFrom = new JTextField();
        txtTimeSlotFrom.setToolTipText("Format: YYYYMMDDHHmm");
        txtTimeSlotFrom.setText(new SimpleDateFormat("YYYYMMddHH00").format(new Date(new Date().getTime() + 2*24*60*60*60)));
        p_TimeSlotFrom.add(txtTimeSlotFrom);
        txtTimeSlotFrom.setColumns(10);
        
        JPanel p_TimeSlotTo = new JPanel();
        p_TimeSlot.add(p_TimeSlotTo);
        p_TimeSlotTo.setLayout(new BoxLayout(p_TimeSlotTo, BoxLayout.X_AXIS));
        
        JLabel lblTimeslotTo = new JLabel("TimeSlot To:");
        lblTimeslotTo.setMinimumSize(new Dimension(150, 14));
        lblTimeslotTo.setMaximumSize(new Dimension(150, 14));
        lblTimeslotTo.setPreferredSize(new Dimension(150, 14));
        p_TimeSlotTo.add(lblTimeslotTo);
        
        txtTimeSlotTo = new JTextField();
        txtTimeSlotTo.setToolTipText("Format: YYYYMMDDHHmm");
        txtTimeSlotTo.setText(new SimpleDateFormat("YYYYMMddHH00").format(new Date(new Date().getTime() + 3*24*60*60*60)));
        p_TimeSlotTo.add(txtTimeSlotTo);
        txtTimeSlotTo.setColumns(10);
        
        JPanel p_WWIDENT = new JPanel();
        p_TimeSlot.add(p_WWIDENT);
        p_WWIDENT.setLayout(new BoxLayout(p_WWIDENT, BoxLayout.X_AXIS));
        
        JLabel lblNewLabel_3 = new JLabel("wwIdent:");
        lblNewLabel_3.setMinimumSize(new Dimension(150, 14));
        lblNewLabel_3.setMaximumSize(new Dimension(150, 14));
        lblNewLabel_3.setPreferredSize(new Dimension(150, 14));
        p_WWIDENT.add(lblNewLabel_3);
        
        txtWWident = new JTextField();
        txtWWident.setText("320509");
        p_WWIDENT.add(txtWWident);
        txtWWident.setColumns(10);
        
        couponList = new ArrayList<>();
        
        JPanel p_Coupons = new JPanel();
        p_Coupons.setPreferredSize(new Dimension(3440, 75));
        p_Coupons.setMinimumSize(new Dimension(3440, 75));
        p_Coupons.setMaximumSize(new Dimension(3440, 75));
        p_data.add(p_Coupons);
        p_Coupons.setLayout(new GridLayout(0, 1, 0, 0));
        
        JPanel p_AddCoupons = new JPanel();
        p_Coupons.add(p_AddCoupons);
        p_AddCoupons.setMinimumSize(new Dimension(3440, 40));
        p_AddCoupons.setMaximumSize(new Dimension(3440, 40));
        p_AddCoupons.setPreferredSize(new Dimension(3440, 40));
        p_AddCoupons.setLayout(new BoxLayout(p_AddCoupons, BoxLayout.X_AXIS));
        
        JLabel lblCoupons = new JLabel("Coupons:");
        lblCoupons.setPreferredSize(new Dimension(150, 14));
        lblCoupons.setMinimumSize(new Dimension(150, 14));
        lblCoupons.setMaximumSize(new Dimension(150, 14));
        p_AddCoupons.add(lblCoupons);
        
        txtEan = new JTextField();
        txtEan.setText("ean1");
        txtEan.setToolTipText("EAN");
        p_AddCoupons.add(txtEan);
        txtEan.setColumns(10);
        
        txtText = new JTextField();
        txtText.setText("%");
        txtText.setToolTipText("Text");
        p_AddCoupons.add(txtText);
        txtText.setColumns(10);
        
        txtType = new JTextField();
        txtType.setText("20% discount");
        txtType.setToolTipText("Type");
        p_AddCoupons.add(txtType);
        txtType.setColumns(10);
        JButton btnAddCoupon = new JButton("Add Coupon");
        btnAddCoupon.setPreferredSize(new Dimension(150, 50));
        btnAddCoupon.setMinimumSize(new Dimension(150, 50));
        btnAddCoupon.setMaximumSize(new Dimension(150, 50));
        p_AddCoupons.add(btnAddCoupon);
        
        JPanel panel = new JPanel();
        p_Coupons.add(panel);
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        
        JLabel lblCouponOverview = new JLabel("Coupons Overview:");
        lblCouponOverview.setPreferredSize(new Dimension(150, 14));
        lblCouponOverview.setMinimumSize(new Dimension(150, 14));
        lblCouponOverview.setMaximumSize(new Dimension(150, 14));
        panel.add(lblCouponOverview);
        
        JComboBox<String> cb_CouponAdded = new JComboBox<>();
        panel.add(cb_CouponAdded);
        
        JButton btnDeleteCoupon = new JButton("Delete Coupon");
        btnDeleteCoupon.setPreferredSize(new Dimension(150, 50));
        btnDeleteCoupon.setMinimumSize(new Dimension(150, 50));
        btnDeleteCoupon.setMaximumSize(new Dimension(150, 50));
        panel.add(btnDeleteCoupon);
        
        JPanel p_MerchInfo = new JPanel();
        p_MerchInfo.setPreferredSize(new Dimension(3440, 40));
        p_MerchInfo.setMaximumSize(new Dimension(3440, 40));
        p_MerchInfo.setMinimumSize(new Dimension(3440, 40));
        p_data.add(p_MerchInfo);
        p_MerchInfo.setLayout(new BoxLayout(p_MerchInfo, BoxLayout.X_AXIS));
        
        JLabel lblNewLabel = new JLabel("MerchInfo:");
        lblNewLabel.setMinimumSize(new Dimension(150, 14));
        lblNewLabel.setMaximumSize(new Dimension(150, 14));
        lblNewLabel.setPreferredSize(new Dimension(150, 14));
        p_MerchInfo.add(lblNewLabel);

        merchLists = new ArrayList<>(sql.getMerchLists());
        cb_MerchInfo = new JComboBox<>(merchLists.toArray());
        p_MerchInfo.add(cb_MerchInfo);
        
        JButton btnPrepareAndTest = new JButton("build JSON");
        btnPrepareAndTest.setMaximumSize(new Dimension(3440, 23));
        btnPrepareAndTest.setMinimumSize(new Dimension(20000, 23));
        btnPrepareAndTest.setPreferredSize(new Dimension(3440, 23));
        p_data.add(btnPrepareAndTest);

        JPanel p_Result = new JPanel();
        p_data.add(p_Result);
        p_Result.setLayout(new GridLayout(0, 1, 0, 0));
        
        JScrollPane sp_Result = new JScrollPane();
        p_Result.add(sp_Result);
        
        txtResultJson = new JTextArea();
        sp_Result.setViewportView(txtResultJson);

        List<List<String>> itemList = sql.getArticleList();
        for(List l : itemList){
            cb_Items.addItem(l.get(0) + " " + l.get(1) +" ("+ l.get(7) +") ("+ l.get(2) +")");
        }

        addedItemList = new ArrayList<>();
        btnAdd.addActionListener(e -> {
            if(itemList.get(cb_Items.getSelectedIndex()).get(itemList.get(cb_Items.getSelectedIndex()).size()-1).equals("false")){
                if(txt_qty.getText().equals("")){
                    txt_qty.setText("0");
                }
                //set quantity
                itemList.get(cb_Items.getSelectedIndex()).set(5,txt_qty.getText());
                System.out.println();
                //add to cb
                cb_AddedItems.addItem(cb_Items.getSelectedItem()+"");
                //add to replace in order
                addedItemList.add(itemList.get(cb_Items.getSelectedIndex()));
                //set last value to true to avoid duplicate entries of the same product
                itemList.get(cb_Items.getSelectedIndex()).set(itemList.get(cb_Items.getSelectedIndex()).size()-1, "true");
            }
        });

        btnRemove.addActionListener(e -> {
            int ele = cb_AddedItems.getSelectedIndex();
            for (int i = 0; i< cb_Items.getItemCount();i++){
                if(cb_Items.getItemAt(i).equals(cb_AddedItems.getSelectedItem())){
                    //remove item from article drop down
                    cb_AddedItems.removeItemAt(ele);
                    // if list contains only 1 element remove x+1 element else remove x element
                    addedItemList.remove(ele);
                    itemList.get(i).set(itemList.get(i).size()-1, "false");
                    i = cb_Items.getItemCount()+1;
                    System.out.println(addedItemList);
                }
            }
        });

        btnModify.addActionListener(e -> {
            //cb_Items;
            List<String> l = new ArrayList<>();
            for (int i = 0; i < cb_AddedItems.getItemCount(); i++){
                if(i == cb_AddedItems.getSelectedIndex()){
                    l.add(cb_Items.getSelectedItem()+"");
                }else {
                    l.add(cb_AddedItems.getItemAt(i)+"");
                }
            }
            cb_AddedItems.removeAllItems();
            for(String s : l){
                cb_AddedItems.addItem(s);
            }
            //set quantity
            itemList.get(cb_Items.getSelectedIndex()).set(6,txt_qty.getText());
            addedItemList.set(cb_AddedItems.getSelectedIndex(),itemList.get(cb_Items.getSelectedIndex()));
            System.out.println(addedItemList);
        });

        btnAddCoupon.addActionListener(e -> {
            List<String> coupon = new ArrayList<>();
            coupon.add(txtEan.getText());
            coupon.add(txtText.getText());
            coupon.add(txtType.getText());
            couponList.add(coupon);
            cb_CouponAdded.addItem(coupon+"");
        });

        btnDeleteCoupon.addActionListener(e -> {
            int select = cb_CouponAdded.getSelectedIndex();
            cb_CouponAdded.removeItemAt(select);
            couponList.remove(select);
            System.out.println(select);
            cb_CouponAdded.repaint();
        });

        btnPrepareAndTest.addActionListener(e -> {
          copyToJSONBuilder();
          txtResultJson.setText(work.toPrettyFormat(jsbo.getOrder().toString().replaceAll("\\{}","")));
        });

        // Address panel in data tab------------------------------------------------------------------------------------

        JPanel p_OrderCreateModify = new JPanel();
        tabbedPane_1.addTab("Order (create/update)", null, p_OrderCreateModify, null);
        p_OrderCreateModify.setLayout(new GridLayout(1, 0, 0, 0));
        
        JPanel p_OrderDetailsHistory = new JPanel();
        tabbedPane_1.addTab("Order (details/history)", null, p_OrderDetailsHistory, null);
        p_OrderDetailsHistory.setLayout(new GridLayout(0, 1, 0, 0));
        
        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        p_OrderDetailsHistory.add(tabbedPane);
        
        JPanel p_OrderResponseBody = new JPanel();
        tabbedPane.addTab("Body", null, p_OrderResponseBody, null);
        p_OrderResponseBody.setLayout(new GridLayout(0, 1, 0, 0));
        //p_OrderResponseBody.setLayout();
        
        JScrollPane scrollPane_4 = new JScrollPane();
        p_OrderResponseBody.add(scrollPane_4);
        
        txt_OrderDetailsBody = new JTextArea();
        scrollPane_4.setViewportView(txt_OrderDetailsBody);
        
        JPanel p_OrderResponseHeader = new JPanel();
        tabbedPane.addTab("Header", null, p_OrderResponseHeader, null);

        JButton btnExecute = new JButton("Execute");
        btnExecute.setPreferredSize(new Dimension(3440, 23));
        cP.add(btnExecute);
        btnExecute.setMinimumSize(new Dimension(20000000, 23));
        btnExecute.setMaximumSize(new Dimension(3440, 23));

        JPanel p_StatusBar = new JPanel();
        p_StatusBar.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
        p_StatusBar.setMaximumSize(new Dimension(2000000, 200));
        cP.add(p_StatusBar);
        GridBagLayout gbl_p_StatusBar = new GridBagLayout();
        gbl_p_StatusBar.columnWidths = new int[] {50, 500, 0};
        gbl_p_StatusBar.rowHeights = new int[]{14, 0};
        gbl_p_StatusBar.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
        gbl_p_StatusBar.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        p_StatusBar.setLayout(gbl_p_StatusBar);

        JLabel lblStatus = new JLabel("Status: ");
        GridBagConstraints gbc_lblStatus = new GridBagConstraints();
        gbc_lblStatus.fill = GridBagConstraints.BOTH;
        gbc_lblStatus.insets = new Insets(0, 0, 0, 5);
        gbc_lblStatus.gridx = 0;
        gbc_lblStatus.gridy = 0;
        p_StatusBar.add(lblStatus, gbc_lblStatus);

        lblMessage = new JLabel("Message");

        GridBagConstraints gbc_lblMessage = new GridBagConstraints();
        gbc_lblMessage.fill = GridBagConstraints.BOTH;
        gbc_lblMessage.gridx = 1;
        gbc_lblMessage.gridy = 0;
        p_StatusBar.add(lblMessage, gbc_lblMessage);
        work.setStatusBar(lblMessage);

        //--------------------------------------------------------------------------------------------------------------
        epm.rdbtnGet.doClick();
        m.rdbtnmntmReweorderservicesitpre.doClick();
        //--------------------------------------------------------------------------------------------------------------
        //bitbucket -> Jenkins test
        btnExecute.addActionListener(e -> work.ExecuteButton(a, epm, h, m, tabbedPane_1, tabbedPane, txt_OrderDetailsBody, txtResultJson, btnExecute, btnPrepareAndTest, lblMessage));
    }

    private void copyToJSONBuilder(){
        jsbo.addedItemList = addedItemList;
        jsbo.couponlists = couponList;
        jsbo.deliverList = fulladdresslist.get(cb_Shipping.getSelectedIndex());
        jsbo.invoiceList = fulladdresslist.get(cb_Invoice.getSelectedIndex());
        jsbo.merchList = merchLists.get(cb_MerchInfo.getSelectedIndex());
        // timeslot
        List<String> tmpTimeList = new ArrayList<>();
        tmpTimeList.add(txtTimeSlotFrom.getText());
        tmpTimeList.add(txtTimeSlotTo.getText());
        tmpTimeList.add(txtWWident.getText());
        jsbo.timeslotList = tmpTimeList;
    }

    static void setResponseText(int origin, String text){
        MyResponsePanel.tp_response.setSelectedIndex(origin);
        JScrollPane sp_txtArea = (JScrollPane) MyResponsePanel.tp_response.getSelectedComponent();
        JTextArea txtArea = (JTextArea) sp_txtArea.getViewport().getView();
        txtArea.setText(text);
    }
}