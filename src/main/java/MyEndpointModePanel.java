import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

class MyEndpointModePanel extends JPanel {
    final JTextField txtOrderID;
    String endpoint = "/orders";
    final JRadioButton rdbtnOrders;
    final JRadioButton rdbtnOrdersorderid;
    final JRadioButton rdbtnGet;
    final JRadioButton rdbtnPost;
    final JRadioButton rdbtnPut;
    final JRadioButton rdbtnDelete;

    MyEndpointModePanel(JPanel p_Allgemein){
        JPanel p_EndpointMode = new JPanel();
        p_EndpointMode.setMinimumSize(new Dimension(10, 100));
        p_EndpointMode.setMaximumSize(new Dimension(32767, 100));
        p_Allgemein.add(p_EndpointMode);
        p_EndpointMode.setLayout(new GridLayout(0, 1, 1, 1));

        JPanel p_Endpoint = new JPanel();
        p_Endpoint.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
        p_EndpointMode.add(p_Endpoint);
        p_Endpoint.setLayout(new GridLayout(0, 4, 0, 0));

        JLabel lblEnpoint = new JLabel("  Enpoint: ");
        lblEnpoint.setHorizontalAlignment(SwingConstants.LEADING);
        p_Endpoint.add(lblEnpoint);

        rdbtnOrders = new JRadioButton("/orders");
        rdbtnOrders.setSelected(true);
        p_Endpoint.add(rdbtnOrders);
        ButtonGroup buttonGroupEnpoint = new ButtonGroup();
        buttonGroupEnpoint.add(rdbtnOrders);

        rdbtnOrdersorderid = new JRadioButton("/orders/{orderid}");
        p_Endpoint.add(rdbtnOrdersorderid);
        buttonGroupEnpoint.add(rdbtnOrdersorderid);

        txtOrderID = new JTextField();
        txtOrderID.setEnabled(false);
        txtOrderID.setBackground(Color.LIGHT_GRAY);
        p_Endpoint.add(txtOrderID);
        txtOrderID.setColumns(10);

        JPanel p_Mode = new JPanel();
        p_Mode.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
        p_EndpointMode.add(p_Mode);
        p_Mode.setLayout(new GridLayout(1, 0, 0, 0));

        JLabel lblMode = new JLabel("  Mode: ");
        lblMode.setHorizontalAlignment(SwingConstants.LEADING);
        p_Mode.add(lblMode);

        rdbtnGet = new JRadioButton("GET");
        ButtonGroup buttonGroupMode = new ButtonGroup();
        buttonGroupMode.add(rdbtnGet);
        rdbtnGet.setSelected(true);
        p_Mode.add(rdbtnGet);

        rdbtnPost = new JRadioButton("POST");
        buttonGroupMode.add(rdbtnPost);
        p_Mode.add(rdbtnPost);

        rdbtnPut = new JRadioButton("PUT");
        rdbtnPut.setEnabled(false);
        buttonGroupMode.add(rdbtnPut);
        p_Mode.add(rdbtnPut);

        rdbtnDelete = new JRadioButton("DELETE");
        rdbtnDelete.setEnabled(false);
        buttonGroupMode.add(rdbtnDelete);
        p_Mode.add(rdbtnDelete);

        rdbtnOrders.addActionListener(arg0 -> {
            endpoint="/orders";
            rdbtnGet.setEnabled(true);
            rdbtnGet.doClick();
            rdbtnPost.setEnabled(true);
            rdbtnPut.setEnabled(false);
            rdbtnDelete.setEnabled(false);
            txtOrderID.setEnabled(false);
            txtOrderID.setBackground(Color.LIGHT_GRAY);
        });

        rdbtnOrdersorderid.addActionListener(arg0 -> {
            rdbtnGet.setEnabled(true);
            rdbtnGet.doClick();
            rdbtnPost.setEnabled(false);
            rdbtnPut.setEnabled(true);
            rdbtnDelete.setEnabled(true);
            txtOrderID.setEnabled(true);
            txtOrderID.setBackground(Color.WHITE);
        });
    }
}
