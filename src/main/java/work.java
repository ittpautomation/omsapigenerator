import com.google.gson.*;
import org.apache.http.*;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

class work {
    private static JLabel statusbar = null;
    private static String responseBody = "";
    private static String environment = "prepre";

    private static String getURLfromEnvironment(String requestType, String env){
        String result = "";
        if(requestType.equals("mToken") || requestType.equals("uToken")){
            switch(env){
                case "preint":
                    result = "http://rewe-authentication.service.int.rewe-digital.com/api/token";
                    break;
                case "prepre":
                    result = "http://rewe-authentication.service.pre.rewe-digital.com/api/token";
                    break;
                case "sitpre":
                    result = "http://rewe-authentication.service.pre.rewe-digital.com/api/token";
                    break;
            }
        }
        if(requestType.equals("bToken")){
            switch(env){
                case "preint":
                    result = "http://rewe-basket.service.int.rewe-digital.com/api/baskets";
                    break;
                case "prepre":
                    result = "http://rewe-basket.service.pre.rewe-digital.com/api/baskets";
                    break;
                case "sitpre":
                    result = "http://rewe-basket.service.pre.rewe-digital.com/api/baskets";
                    break;
            }
        }
        if(requestType.equals("pToken")){
            switch(env){
                case "preint":
                    result = "http://payment-reservation-service.service.int.rewe-digital.com/api/paymentrequests";
                    break;
                case "prepre":
                    result = "http://payment-reservation-service.service.pre.rewe-digital.com/api/paymentrequests";
                    break;
                case "sitpre":
                    result = "http://payment-reservation-service.service.pre.rewe-digital.com/api/paymentrequests";
                    break;
            }
        }
        if(requestType.equals("order")){
            switch(env){
                case "preint":
                    result = "http://rewe-order-service.service.int.rewe-digital.com/api";
                    break;
                case "prepre":
                    result = "http://rewe-order-service.service.pre.rewe-digital.com/api";
                    break;
                case "sitpre":
                    result = "http://rewe-order-service-sit.service.pre.rewe-digital.com/api";
                    break;
            }
        }
        return result;
    }

    static List<String> mToken(List<Header> header) {
        String url = getURLfromEnvironment("mToken", environment);

        List<NameValuePair> elements = Arrays.asList(
                new BasicNameValuePair("grant_type", "client_credentials"),
                new BasicNameValuePair("scope", "read")
        );

        List<String> returnValues = new ArrayList<>();
        returnValues.add("access_token");

        return restRequest(url, header, elements, "", returnValues,0);
    }

    static List<String> uToken(List<Header> header, String txtUserTokenUser, String txtUserTokenPW) {
        String url = getURLfromEnvironment("uToken", environment);

        List<NameValuePair> elements = Arrays.asList(
                new BasicNameValuePair("grant_type", "password"),
                new BasicNameValuePair("scope", "read"),
                new BasicNameValuePair("username", txtUserTokenUser),
                new BasicNameValuePair("password", txtUserTokenPW)
        );

        List<String> returnValues = new ArrayList<>();
        returnValues.add("access_token");
        returnValues.add("user_id");

        return restRequest(url, header, elements, "", returnValues,1);
    }

    static List<String> basketID(List<Header> header, String userID){
        String url = getURLfromEnvironment("bToken", environment);

        List<NameValuePair> elements = Collections.emptyList();

        List<String> returnValues = new ArrayList<>();
        returnValues.add("id");

        String body = "{\"customerUUID\": \""+userID+"\",\"version\": 1,\"storeId\": \"320509\",\"serviceType\": \"DELIVERY\",\"currencyCode\": \"EUR\",\"zipCode\": \"50670\",\"lineItems\": [{\"lineItemType\": \"PRODUCT\",\"product\": {\"articleId\": \"4250241203517\",\"productId\": \"3645517\",\"gtin\": \"4250241203517\",\"nan\": \"3645517\",\"version\": 1,\"title\": \"fs\",\"price\": 10,\"refundPrice\": 0,\"volumeCode\": \"STK\",\"basePrice\": 13,\"baseAmount\": 1,\"baseMeasure\": \"KG\",\"baseQuantity\": 7.0,\"bulkyGoodsShare\": 0,\"orderLimit\": 17,\"orderLimitForPromotions\": 80,\"imageUrl\": \"https://d9gkwhfwam31p.cloudfront.net/6422551/3748960_warenkorb.png\"},\"quantity\": 4,\"category\": {\"id\": \"1103\",\"name\": \"Tiefkühl\"}}]}";

        return restRequest(url, header, elements , body, returnValues,2);
    }

    static List<String> paymentToken(List<Header> header, String uuid, String email, JsonBuilderOMS jsbo){
        String url = getURLfromEnvironment("pToken", environment);

        List<NameValuePair> elements = Collections.emptyList();

        List<String> returnValues = new ArrayList<>();
        returnValues.add("paymentRequestId");

        jsbo.uuid = uuid;
        jsbo.email = email;

        String body = jsbo.getPaymentTokenBody().toString();
        //run first time to get payment id and generate with it request url and body for the prepare payment request
        url = url + "/" +  restRequest(url, header, elements , body, returnValues,3).get(0) + "/paymentmethods/INVOICE";
        body = "{\"birthday\": \"2018-02-20T13:02:40.359Z\"}";

        return restRequest(url, header, elements , body, returnValues,3);
    }

    private static List<String> createOrder(String url, List<Header> header, String body){
        List<String> returnValues = new ArrayList<>();
        returnValues.add("id");
        return restRequest(url, header, Collections.emptyList(), body, returnValues,9);
    }

    private static List<String> restRequest(String url, List<Header> header, List<NameValuePair> elements, String body, List<String> returnElement, int origin){
        List<String> result = new ArrayList<>();
        try (CloseableHttpClient httpClient = HttpClients.custom().setDefaultHeaders(header).build()){
            HttpPost postRequest = new HttpPost(url);
            if(!Objects.equals(body, "")){
                HttpEntity entity = new ByteArrayEntity(body.getBytes("UTF-8"));
                postRequest.setEntity(entity);
            }else if(elements.size() != 0) {
                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(elements, Consts.UTF_8);
                postRequest.setEntity(entity);
            }
            System.out.println("POSTRequest: " + postRequest);
            System.out.println("HEADER:      " + header);
            System.out.println("ELEMENTS:    " + elements);
            //System.out.println("PARAMS:      " + params);
            System.out.println("BODY:        " + body.replaceAll("\n",""));
            responseBody="";
            ResponseHandler<String> responseHandler = response -> {
                int status = response.getStatusLine().getStatusCode();
                statusbar.setText(status+" ");
                if (status >= 200 && status < 300 || status == 409 || (status == 403 && url.endsWith("paymentmethods/INVOICE") )) {
                    if(status == 403){
                        return "{\"paymentRequestId\": \""+url.replaceAll("/paymentmethods/INVOICE","").split("/")[5]+"\",\n\"fake\":\"value\"}";
                    }
                    HttpEntity responseEntity = response.getEntity();
                    return responseEntity != null ? EntityUtils.toString(responseEntity) : null;
                } else {
                    System.out.println(responseBody);
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            };
            responseBody = httpClient.execute(postRequest, responseHandler);
            JSONObject obj = new JSONObject(responseBody);
            System.out.println(obj);
            for (String e : returnElement){
                result.add(obj.getString(e));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("ResponseValues: " + result);
        if(origin != 9){
            StartFrame.setResponseText(origin, toPrettyFormat(responseBody));
        }
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------");
        return result;
    }

    private static List<String> deleteRequest(String url, List<Header> header, List<String> returnElement){
        List<String> result = new ArrayList<>();
        try (CloseableHttpClient httpClient = HttpClients.custom().setDefaultHeaders(header).build()){
            HttpDelete deleteRequest = new HttpDelete(url);
            System.out.println("POSTRequest: " + deleteRequest);
            System.out.println("HEADER:      " + header);
            responseBody="";
            ResponseHandler<String> responseHandler = response -> {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300 || status == 409 || (status == 403 && url.endsWith("paymentmethods/INVOICE") )) {
                    HttpEntity responseEntity = response.getEntity();
                    statusbar.setText("Order was deleted successfully");
                    return responseEntity != null ? EntityUtils.toString(responseEntity) : null;
                } else {
                    System.out.println(responseBody);
                    statusbar.setText("Order was NOT deleted");
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            };
            responseBody = httpClient.execute(deleteRequest, responseHandler);
            System.out.println(responseBody);
            if(!responseBody.equals("")){
                JSONObject obj = new JSONObject(responseBody);
                System.out.println(obj);
                for (String e : returnElement){
                    result.add(obj.getString(e));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("ResponseValues: " + result);
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------");
        return result;
    }

    private static String orderhistory(String url, String userToken, String userId) {
      List<String> result = new ArrayList<>();
      List<Header> header = Arrays.asList(
              new BasicHeader("auth-info-user-id", userId),
              new BasicHeader("Authorization", "Bearer " + userToken),
              new BasicHeader("Content-Type","application/json")
      );
      HttpClient client = HttpClients.custom().setDefaultHeaders(header).build();
      System.out.println(url);
      HttpGet request = new HttpGet(url);
      String Rresult = "";
      try {
          HttpResponse response = client.execute(request);
          HttpEntity entity = response.getEntity();
          if (entity != null) {
              statusbar.setText(response.getStatusLine().getStatusCode()+" ");
              try (InputStream stream = entity.getContent()) {
                  BufferedReader reader =
                          new BufferedReader(new InputStreamReader(stream));
                  String line;
                  while ((line = reader.readLine()) != null) {
                      Rresult = Rresult.concat(line).concat("\n");
                      System.out.println(line);
                  }
              }
          }
      } catch (IOException e) {
          e.printStackTrace();
          System.out.println(result);
      }
      return Rresult;
  }

    static void setStatusBar(JLabel jtf){
        statusbar = jtf;
    }

    static void putResultToField(List<String> values, JTextField jTxtField,JTextField jTxtField2){
        if(values.size() == 2 && jTxtField2 != null){
            jTxtField.setText(values.get(0));
            jTxtField2.setText(values.get(1));
        }else{
            jTxtField.setText(values.get(0));
        }
    }

    static void putResultToField(List<String> values, JTextField jTxtField){
        putResultToField(values, jTxtField,null);
    }

    static List<Header> getHeaderFromTable(JTable table, String UserTokenUsername, String userID, String MachineToken, String UserToken){
        List<Header> l = new ArrayList<>();
        int rows;
        String value;
        rows = table.getModel().getRowCount();
        for (int i = 0; i<rows;i++){
            if(table.getValueAt(i,1) != null){
                value = table.getValueAt(i,1).toString();
                value = value.replaceAll("<replaceUsernameFromUserToken>",UserTokenUsername);
                value = value.replaceAll("<replaceUserID>",userID);
                value = value.replaceAll("<replaceTokenMachineOrUser>",!MachineToken.equals("") ? MachineToken : UserToken);
                l.add(new BasicHeader(table.getValueAt(i,0).toString(),value));
            }
        }
        System.out.println(l);
        return l;
    }

    static String toPrettyFormat(String jsonString) {
        String result;
        try{
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            JsonParser jp = new JsonParser();
            JsonElement je = jp.parse(jsonString);
            result = gson.toJson(je);
        }catch(Exception e){
            e.printStackTrace();
            result = "";
        }
        return result;
    }

    private static String getEnvironemnt(){
        return environment;
    }

    static void setEnvironment(String environment) {
      work.environment = environment;
    }

    static void ExecuteButton(MyAuthPanel a, MyEndpointModePanel epm, MyHeaderPanel h,MyMenu m, JTabbedPane tabbedPane_1, JTabbedPane tabbedPane, JTextArea txt_OrderDetailsBody, JTextArea txtResultJson, JButton btnExecute, JButton btnPrepareAndTest, JLabel lblMessage){
        try {
            if (((a.txtUserToken.getText().equals("") && a.txtMachineToken.getText().equals("")) && a.txtUserID.getText().equals("")) || m.jcbmiGenerateNew.getState()) {
                a.btnGetUserToken.doClick();
            }
            if (a.txtMachineToken.getText().equals("") || m.jcbmiGenerateNew.getState()) {
                a.btnGetMachinetoken.doClick();
            }

            String endpoint_f;
            if (epm.rdbtnOrders.isSelected()) {
                if (epm.rdbtnGet.isSelected()) {
                    endpoint_f = epm.endpoint;
                    tabbedPane_1.setSelectedIndex(3); //select OrderHistory/Details tab
                    tabbedPane.setSelectedIndex(0); //select Body tab
                    txt_OrderDetailsBody.setText(work.orderhistory(work.getURLfromEnvironment("order", work.getEnvironemnt()).concat(endpoint_f), a.txtUserToken.getText(), a.txtUserID.getText()));
                }
                if (epm.rdbtnPost.isSelected()) {
                    if (txtResultJson.getText().equals("") || m.jcbmiGenerateNew.getState()) {
                        btnPrepareAndTest.doClick();
                    }
                    endpoint_f = epm.endpoint;
                    epm.txtOrderID.setText(work.createOrder(work.getURLfromEnvironment("order", work.getEnvironemnt()).concat(endpoint_f), work.getHeaderFromTable(h.t_orderCreateModify, a.txtUserTokenUser.getText(), a.txtUserID.getText(), a.txtMachineToken.getText(), a.txtUserToken.getText()), txtResultJson.getText()).get(0));
                    epm.rdbtnOrdersorderid.doClick();
                    epm.rdbtnGet.doClick();
                    btnExecute.doClick();
                }
            } else if (epm.rdbtnOrdersorderid.isSelected() && !epm.txtOrderID.getText().equals("")) {
                if (epm.rdbtnGet.isSelected()) {
                    endpoint_f = epm.endpoint + "/" + epm.txtOrderID.getText();
                    tabbedPane_1.setSelectedIndex(3); //select OrderHistory/Details tab
                    tabbedPane.setSelectedIndex(0); //select Body tab
                    txt_OrderDetailsBody.setText(work.orderhistory(work.getURLfromEnvironment("order", work.getEnvironemnt()).concat(endpoint_f), a.txtUserToken.getText(), a.txtUserID.getText()));
                    JSONObject resultJson = new JSONObject(StartFrame.txt_OrderDetailsBody.getText());
                    statusbar.setText("csId: " + JsonBuilderOMS.getCsIdValue(resultJson) + " / Status: " + resultJson.getString("status") + " / OrderId: " + resultJson.getString("orderId"));
                }
                if (epm.rdbtnPut.isSelected()) {
                    //post order update
                }
                if (epm.rdbtnDelete.isSelected()) {
                    endpoint_f = epm.endpoint + "/" + epm.txtOrderID.getText();
                    System.out.println(work.deleteRequest(work.getURLfromEnvironment("order", work.getEnvironemnt()).concat(endpoint_f), work.getHeaderFromTable(h.t_orderCreateModify, a.txtUserTokenUser.getText(), a.txtUserID.getText(), a.txtMachineToken.getText(), a.txtUserToken.getText()), new ArrayList<>()));

                }
            } else {
                lblMessage.setText("Bitte gib eine OrderID ein!");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
