import com.mysql.cj.xdevapi.JsonArray;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

class JsonBuilderOMS {
    List<List<String>> addedItemList;
    List<List<String>> couponlists;
    List<String> merchList;
    List<String> deliverList;
    List<String> invoiceList;
    List<String> timeslotList;

    JsonBuilderOMS(){

    }

    JSONObject getOrder(){
        JSONObject jsnorder = new JSONObject();
        if((Integer.parseInt(timeslotList.get(0).substring(0,8)) >= Integer.parseInt(new SimpleDateFormat("YYYYMMdd").format(new Date()))) && (Integer.parseInt(timeslotList.get(1).substring(0,8)) >= Integer.parseInt(new SimpleDateFormat("YYYYMMdd").format(new Date())))){
            jsnorder.put("marketCode", "320509");
            jsnorder.put("clientInfo","iOS 9.0.1 AppVersion 2.0");
            jsnorder.put("payment",getPayment());
            jsnorder.put("customerUUID","some-customer-uuid");
            jsnorder.put("basketUUID", UUID.randomUUID());
            jsnorder.put("paybackInfo",getPaybackInfo());
            //jsnorder.put("customerInfo","");
            //jsnorder.put("lastModified","");
            //jsnorder.put("lastStatusUpdate","");
            jsnorder.put("subOrders",getSuborder());
            jsnorder.put("creationDate",new SimpleDateFormat("YYYYMMddhhmm").format(new Date()));
            jsnorder.put("invoiceAddress",getInvoiceAddress("order"));
            jsnorder.put("orderValue", getOrderValue(jsnorder));
            jsnorder.put("refundPrice", (int)(jsnorder.getInt("orderValue")*0.9));
            StartFrame.lblMessage.setText(jsnorder.getInt("orderValue") >= 4000 ? "Min order Value reached" : "Min order value not reached (missing " + (4000-jsnorder.getInt("orderValue")) + " of order total)");
        }
        return jsnorder;
    }

    private List<JSONObject> getLineItems(){
        List<JSONObject> result = new ArrayList<>();
        for (int i = 0; i < addedItemList.size(); i++){
            List l = addedItemList.get(i);
            switch(l.get(0).toString()){
                case "PRODUCT":
                    JSONObject jsnarticle = new JSONObject();
                    jsnarticle.put("sequenceNumber", i);
                    jsnarticle.put("totalPrice", Integer.parseInt(l.get(5).toString()) *Integer.parseInt(l.get(6).toString()));
                    //jsnarticle.put("totalRefundPrice", Integer.parseInt(l.get(6).toString()) *Integer.parseInt(l.get(8).toString()));
                    jsnarticle.put("lineItemType",l.get(0));
                    jsnarticle.put("productId",l.get(1));
                    jsnarticle.put("articleId",l.get(2));
                    jsnarticle.put("gtin",l.get(3));
                    jsnarticle.put("nan",l.get(4));
                    //jsnarticle.put("version", Integer.parseInt(l.get(5).toString()));
                    jsnarticle.put("quantity", Integer.parseInt(l.get(5).toString()));
                    jsnarticle.put("price", Integer.parseInt(l.get(6).toString()));
                    //jsnarticle.put("refundPrice", Integer.parseInt(l.get(8).toString()));
                    //jsnarticle.put("discount", Integer.parseInt(l.get(9).toString()));
                    //jsnarticle.put("discountValidFrom",l.get(10));
                    //jsnarticle.put("discountValidTo",l.get(11));
                    //jsnarticle.put("imageUrl",l.get(12));
                    //jsnarticle.put("regularPrice", Integer.parseInt(l.get(13).toString()));
                    jsnarticle.put("title",l.get(7));
                    jsnarticle.put("volumeCode",l.get(8));
                    jsnarticle.put("weightPerPiece", Integer.parseInt(l.get(9).toString()));
                    //jsnarticle.put("basePrice", Integer.parseInt(l.get(17).toString()));
                    jsnarticle.put("pricePerKilo", Integer.parseInt(l.get(10).toString()));
                    //jsnarticle.put("baseAmount", Integer.parseInt(l.get(19).toString()));
                    //jsnarticle.put("baseMeasure",l.get(20));
                    //jsnarticle.put("baseQuantity",Double.parseDouble(l.get(21).toString()));
                    //jsnarticle.put("multi1", Integer.parseInt(l.get(22).toString()));
                    //jsnarticle.put("multi2", Integer.parseInt(l.get(23).toString()));
                    //jsnarticle.put("percentageOfAlcoholByVolume",l.get(24));
                    //jsnarticle.put("quantityType",l.get(25));
                    //jsnarticle.put("brand",l.get(26));
                    //jsnarticle.put("packaging",l.get(27));
                    //jsnarticle.put("bulkyGoodsShare", Integer.parseInt(l.get(28).toString()));
                    //jsnarticle.put("orderLimit", Integer.parseInt(l.get(29).toString()));
                    //jsnarticle.put("orderLimitForPromotions", Integer.parseInt(l.get(30).toString()));
                    //jsnarticle.put("originalPrice", Integer.parseInt(l.get(31).toString()));
                    //JSONObject jsncategory = new JSONObject();
                    //jsncategory.put("id",l.get(32));
                    //jsncategory.put("name",l.get(33));
                    //jsnarticle.put("category",jsncategory);
                    result.add(jsnarticle);
                    break;
                default:
                    JSONObject jsnobject = new JSONObject();
                    jsnobject.put("sequenceNumber", i);
                    jsnobject.put("lineItemType",l.get(0));
                    jsnobject.put("productId",l.get(1));
                    jsnobject.put("articleId",l.get(2));
                    jsnobject.put("gtin",l.get(3));
                    jsnobject.put("nan",l.get(4));
                    jsnobject.put("quantity", Integer.parseInt(l.get(5).toString()));
                    jsnobject.put("price", Integer.parseInt(l.get(6).toString()));
                    jsnobject.put("refundPrice", Integer.parseInt(l.get(6).toString()));
                    jsnobject.put("totalPrice", Integer.parseInt(l.get(5).toString()) *Integer.parseInt(l.get(6).toString()));
                    result.add(jsnobject);
            }
        }
        return result;
    }

    private JSONArray getSuborder(){
        JSONObject jsnsuborder = new JSONObject();
        jsnsuborder.put("deliveryType", "DELIVERY");
        for (JSONObject jo : getCoupons()){
            jsnsuborder.append("coupons",jo);
        }
        jsnsuborder.put("merchantInfo",getMerchInfo());
        //jsnsuborder.put("csId","");
        jsnsuborder.put("vatItems",new JsonArray());
        jsnsuborder.put("deliveryAddress",getDeliveryAddress("order"));
        for (JSONObject jo : getLineItems()){
            jsnsuborder.append("lineItems",jo);
        }
        jsnsuborder.put("subOrderValue",getSuborderValue(jsnsuborder));
        jsnsuborder.put("articleSubstitution",true);
        jsnsuborder.put("timeSlot",getTimeSlot());
        //jsnsuborder.put("lastStatusUpdate","");
        jsnsuborder.put("userComment","Bitte keine Ersatzartikel");
        JSONArray js = new JSONArray();
        js.put(jsnsuborder);
        return js;
    }

    private JSONObject getTimeSlot(){
        JSONObject jsntimeslot = new JSONObject();
        jsntimeslot.put("firstSlotDate",timeslotList.get(0));
        jsntimeslot.put("lastSlotDate",timeslotList.get(1));
        jsntimeslot.put("wwIdent",timeslotList.get(2));
        return jsntimeslot;
    }

    private List<JSONObject> getCoupons(){
        List<JSONObject> result = new ArrayList<>();
        for (List l : couponlists) {
            JSONObject jsncoupon = new JSONObject();
            jsncoupon.put("ean", l.get(0));
            jsncoupon.put("type", l.get(1));
            jsncoupon.put("text", l.get(2));
            result.add(jsncoupon);
        }
        return result;
    }

    private JSONObject getDeliveryAddress(String usage){
        JSONObject jsndeliveryaddr = new JSONObject();
        jsndeliveryaddr.put("addressId",deliverList.get(0));
        jsndeliveryaddr.put("salutation",deliverList.get(1));
        jsndeliveryaddr.put("firstName",deliverList.get(3));
        jsndeliveryaddr.put("street",deliverList.get(4));
        jsndeliveryaddr.put("houseNumber",deliverList.get(5));
        jsndeliveryaddr.put("zipCode",deliverList.get(6));
        jsndeliveryaddr.put("city",deliverList.get(7));
        jsndeliveryaddr.put("addressAddition",deliverList.get(8));
        jsndeliveryaddr.put("country",deliverList.get(9));
        jsndeliveryaddr.put("companyName",deliverList.get(10));
        jsndeliveryaddr.put("companyAddition1",deliverList.get(11));
        jsndeliveryaddr.put("companyAddition2",deliverList.get(12));
        //jsndeliveryaddr.put("latitude",Double.parseDouble(deliverList.get(13)));
        //jsndeliveryaddr.put("longitude",Double.parseDouble(deliverList.get(14)));
        jsndeliveryaddr.put("phoneNumber",deliverList.get(15));
        if(usage.equals("paymentToken")){
            jsndeliveryaddr.put("surname",deliverList.get(2));
        }else{
            jsndeliveryaddr.put("lastName",deliverList.get(2));
        }
        return jsndeliveryaddr;
    }

    private JSONObject getInvoiceAddress(String usage){
        JSONObject jsninvoiceaddr = new JSONObject();
        jsninvoiceaddr.put("addressId",invoiceList.get(0));
        jsninvoiceaddr.put("salutation",invoiceList.get(1));
        jsninvoiceaddr.put("firstName",invoiceList.get(3));
        jsninvoiceaddr.put("street",invoiceList.get(4));
        jsninvoiceaddr.put("houseNumber",invoiceList.get(5));
        jsninvoiceaddr.put("zipCode",invoiceList.get(6));
        jsninvoiceaddr.put("city",invoiceList.get(7));
        jsninvoiceaddr.put("addressAddition",invoiceList.get(8));
        jsninvoiceaddr.put("country",invoiceList.get(9));
        jsninvoiceaddr.put("companyName",invoiceList.get(10));
        jsninvoiceaddr.put("companyAddition1",invoiceList.get(11));
        jsninvoiceaddr.put("companyAddition2",invoiceList.get(12));
        //jsninvoiceaddr.put("latitude",Double.parseDouble(invoiceList.get(13)));
        //jsninvoiceaddr.put("longitude",Double.parseDouble(invoiceList.get(14)));
        jsninvoiceaddr.put("phoneNumber",invoiceList.get(15));
        if(usage.equals("paymentToken")){
            jsninvoiceaddr.put("surname",invoiceList.get(2));
        }else{
            jsninvoiceaddr.put("lastName",invoiceList.get(2));
            jsninvoiceaddr.put("customerType",invoiceList.get(16));
        }
        return jsninvoiceaddr;
    }

    private JSONObject getMerchInfo(){
        JSONObject jsnmerchinfo = new JSONObject();
        jsnmerchinfo.put("id",merchList.get(0));
        jsnmerchinfo.put("name",merchList.get(1));
        if(!merchList.get(2).equals("")){
            jsnmerchinfo.put("version",merchList.get(2));
        }
        jsnmerchinfo.put("vendorNumber",merchList.get(3));
        jsnmerchinfo.put("type",merchList.get(4));
        return jsnmerchinfo;
    }

    private JSONObject getPayment(){
        JSONObject jsnpayment = new JSONObject();
        jsnpayment.put("paymentRequestId",UUID.randomUUID());
        return jsnpayment;
    }

    private JSONObject getPaybackInfo(){
        JSONObject jsnpaybackinfo = new JSONObject();
        jsnpaybackinfo.put("usePayback",false);
        return jsnpaybackinfo;
    }

    private int getSuborderValue(JSONObject subOrder){
        int result = 0;
        for(Object o: subOrder.getJSONArray("lineItems")){
            if ( o instanceof JSONObject ) {
                result += ((JSONObject) o).getInt("quantity") *((JSONObject) o).getInt("price");
            }
        }
        return result;
    }

    private int getOrderValue(JSONObject order){
        int result = 0;
        for(Object o: order.getJSONArray("subOrders")){
            if ( o instanceof JSONObject ) {
                result += ((JSONObject) o).getInt("subOrderValue");
            }
        }
        return result;
    }

    static String getCsIdValue(JSONObject order){
        String result = "";
        for(Object o: order.getJSONArray("subOrders")){
            if ( o instanceof JSONObject ) {
                try {
                    result += ((JSONObject) o).getString("csId");
                }catch(Exception e){
                    result = "";
                }
            }
        }
        return result;
    }

    String uuid = "";
    String email = "";
    private JSONObject getPaymentTokenCustomer(){
        JSONObject jsnCustomer = new JSONObject();
        jsnCustomer.put("customerType","PERSONAL");
        jsnCustomer.put("customerUUID",uuid);
        jsnCustomer.put("gender","MALE");
        jsnCustomer.put("email",email);
        return jsnCustomer;
    }

    JSONObject getPaymentTokenBody(){
        JSONObject jsnBody = new JSONObject();
        jsnBody.put("marketCode","320509");
        jsnBody.put("shippingAmount",499);
        jsnBody.put("totalOrderAmount",getOrderValue(getOrder()));
        jsnBody.put("invoiceAddress",getDeliveryAddress("paymentToken"));
        jsnBody.put("shippingAddress",getInvoiceAddress("paymentToken"));
        jsnBody.put("customer",getPaymentTokenCustomer());
        return jsnBody;
    }
}
