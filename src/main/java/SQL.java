import java.sql.*;
import java.util.ArrayList;
import java.util.List;

class SQL {
    private Connection connect = null;
    private ResultSet resultSet = null;
    private Statement statement = null;

    public SQL(){

    }

    public boolean connect(){
        connect = null;
        boolean result = false;
        try{
            connect = DriverManager.getConnection("jdbc:mysql://akurosia.de:3307?useSSL=false&serverTimezone=Europe/Berlin", "akurosia", "kamo1990");
            String query = "SHOW DATABASES LIKE 'omsapi';";
            Statement stmt = connect.createStatement();
            ResultSet rset = stmt.executeQuery(query);
            rset.last();
            result = rset.getRow() == 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    public List<List<String>> getArticleList(){
        List<List<String>> l = new ArrayList<>();
        List<String> tmp;
        try{
            statement = connect.createStatement();
            resultSet = statement.executeQuery("Select a.* FROM omsapi.article as a WHERE a.benutz = 1 ORDER BY a.lineitemType");
            while(resultSet.next()){
                tmp = new ArrayList<>();
                for (int i = 1; i<=resultSet.getMetaData().getColumnCount();i++){
                    tmp.add(resultSet.getString(i));
                }
                tmp.add("false");
                l.add(tmp);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return l;
  }

    public List<List<String>> getAddressList(){
        List<List<String>> l = new ArrayList<>();
        List<String> tmp;
        try{
            statement = connect.createStatement();
            resultSet = statement.executeQuery("Select * FROM omsapi.addressen Where benutz = 1");
            while(resultSet.next()){
                tmp = new ArrayList<>();
                for (int i = 1; i<=resultSet.getMetaData().getColumnCount();i++){
                    tmp.add(resultSet.getString(i));
                }
                l.add(tmp);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return l;
    }

    public List<List<String>> getMerchLists(){
        List<List<String>> l = new ArrayList<>();
        List<String> tmp;
        try{
            statement = connect.createStatement();
            resultSet = statement.executeQuery("Select * FROM omsapi.merch");
            while(resultSet.next()){
                tmp = new ArrayList<>();
                for (int i = 1; i<=resultSet.getMetaData().getColumnCount();i++){
                    tmp.add(resultSet.getString(i));
                }
                l.add(tmp);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return l;
    }

    public List<String> getDropDownInformation(List<List<String>> lists){
        List<String> l = new ArrayList<>();
        for (List li : lists){
            String a = "";
            for(int i = 1; i< 8;i++) {
                a = a.concat(li.get(i).toString().concat(" - "));
            }
            a = a.substring(0,a.length()-3);
            l.add(a);
//l.add(resultSet.getString(1) + " - " + resultSet.getString(2) + " - " + resultSet.getString(3));
        }
        return l;
    }
}
