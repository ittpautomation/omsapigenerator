import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

class MyResponsePanel {
    public static JTabbedPane tp_response;
    MyResponsePanel(JPanel p_Allgemein){
        JPanel p_Response = new JPanel();
        p_Response.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
        p_Allgemein.add(p_Response);
        p_Response.setLayout(new GridLayout(0, 1, 0, 0));

        tp_response = new JTabbedPane(JTabbedPane.TOP);
        p_Response.add(tp_response);

        JScrollPane scrollPane = new JScrollPane();
        tp_response.addTab("Machine Response", null, scrollPane, null);

        JTextArea txtA_MachineToken = new JTextArea();
        scrollPane.setViewportView(txtA_MachineToken);

        JScrollPane scrollPane_1 = new JScrollPane();
        tp_response.addTab("User Response", null, scrollPane_1, null);

        JTextArea txtA_userResult = new JTextArea();
        scrollPane_1.setViewportView(txtA_userResult);

        JScrollPane scrollPane_2 = new JScrollPane();
        tp_response.addTab("Basked Responseb", null, scrollPane_2, null);

        JTextArea txtA_basketResult = new JTextArea();
        scrollPane_2.setViewportView(txtA_basketResult);

        JScrollPane scrollPane_3 = new JScrollPane();
        tp_response.addTab("Payment response", null, scrollPane_3, null);

        JTextArea txtA_paymentResult = new JTextArea();
        scrollPane_3.setViewportView(txtA_paymentResult);
    }
}
