import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

class MyHeaderPanel extends JPanel {

    public final JTable t_machineToken;
    public final JTable t_userToken;
    public final JTable t_basketID;
    public final JTable t_paymentToken;
    public final JTable t_orderCreateModify;

    MyHeaderPanel(JPanel p_Allgemein){
        JPanel p_Header = new JPanel();
        p_Header.setPreferredSize(new Dimension(10, 125));
        p_Header.setMaximumSize(new Dimension(32767, 200));
        p_Allgemein.add(p_Header);
        p_Header.setLayout(new GridLayout(0, 1, 0, 0));

        JTabbedPane tp_headerKeys = new JTabbedPane(JTabbedPane.TOP);
        p_Header.add(tp_headerKeys);

        JScrollPane sp_MachineToken = new JScrollPane();
        tp_headerKeys.addTab("Machine", null, sp_MachineToken, null);

        t_machineToken = new JTable();
        sp_MachineToken.setViewportView(t_machineToken);
        t_machineToken.setModel(new DefaultTableModel(
                new Object[][] {
                        {"Authorization", "Basic bXktY2xpZW50LXdpdGgtc2VjcmV0OnNlY3JldA=="},
                        {"Content-Type", "application/x-www-form-urlencoded"},
                },
                new String[] {
                        "Key", "Value"
                }
        ));

        JScrollPane sp_UserToken = new JScrollPane();
        tp_headerKeys.addTab("User", null, sp_UserToken, null);

        t_userToken = new JTable();
        sp_UserToken.setViewportView(t_userToken);
        t_userToken.setModel(new DefaultTableModel(
                new Object[][] {
                        {"Authorization", "Basic bXktY2xpZW50LXdpdGgtc2VjcmV0OnNlY3JldA=="},
                        {"Content-Type", "application/x-www-form-urlencoded"},
                },
                new String[] {
                        "Key", "Value"
                }
        ));

        JScrollPane sp_BasketId = new JScrollPane();
        tp_headerKeys.addTab("Basked", null, sp_BasketId, null);

        t_basketID = new JTable();
        sp_BasketId.setViewportView(t_basketID);
        t_basketID .setModel(new DefaultTableModel(
                new Object[][] {
                        {"Authorization", "Bearer <replaceTokenMachineOrUser>"},
                        {"Content-Type","application/json"},
                },
                new String[] {
                        "Key", "Value"
                }
        ));

        JScrollPane sp_PaymentToken = new JScrollPane();
        tp_headerKeys.addTab("Payment", null, sp_PaymentToken, null);

        t_paymentToken = new JTable();
        sp_PaymentToken.setViewportView(t_paymentToken);
        t_paymentToken.setModel(new DefaultTableModel(
                new Object[][] {
                        {"Authorization", "Bearer <replaceTokenMachineOrUser>"},
                        {"Content-Type","application/json"},
                },
                new String[] {
                        "Key", "Value"
                }
        ));

        JScrollPane sp_OrderCreateModify = new JScrollPane();
        tp_headerKeys.addTab("Order (create/update)", null, sp_OrderCreateModify, null);

        t_orderCreateModify = new JTable();
        sp_OrderCreateModify.setViewportView(t_orderCreateModify);
        t_orderCreateModify.setModel(new DefaultTableModel(
                new Object[][] {
                        {"customerUUID", "<replaceUserID>"},
                        {"Authorization", "Bearer <replaceTokenMachineOrUser>"},
                        {"Content-Type","application/json"},
                        {"customerEmail", "<replaceUsernameFromUserToken>"},
                },
                new String[] {
                        "Key", "Value"
                }
        ));

        JScrollPane sp_OrderDetailsHistory = new JScrollPane();
        tp_headerKeys.addTab("Order (details/ history", null, sp_OrderDetailsHistory, null);

        JTable t_orderDetailsHistory = new JTable();
        sp_OrderDetailsHistory.setViewportView(t_orderDetailsHistory);
        t_orderDetailsHistory.setModel(new DefaultTableModel(
                new Object[][] {
                        {"auth-info-user-id", "<replaceUserID>"},
                        {"Authorization", "Bearer <replaceTokenMachineOrUser>"},
                },
                new String[] {
                        "Key", "Value"
                }
        ));

        JButton btnAddKeyLine = new JButton("Add new Key");
        btnAddKeyLine.addActionListener(arg0 -> {
            JScrollPane test = (JScrollPane) tp_headerKeys.getSelectedComponent();
            JTable test1 = (JTable) test.getViewport().getComponent(0);
            DefaultTableModel model = (DefaultTableModel) test1.getModel();
            model.addRow(new Object[][] {null, null});
        });
        btnAddKeyLine.setPreferredSize(new Dimension(3440, 23));
        btnAddKeyLine.setMinimumSize(new Dimension(20000, 23));
        btnAddKeyLine.setMaximumSize(new Dimension(3440, 23));
        p_Allgemein.add(btnAddKeyLine);
    }
}
