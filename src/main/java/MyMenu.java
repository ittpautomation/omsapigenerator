import javax.swing.*;

class MyMenu extends JMenu {
    private final JRadioButtonMenuItem rdbtnmntmReweorderservicepre;
    public final JRadioButtonMenuItem rdbtnmntmReweorderservicesitpre;
    public final JCheckBoxMenuItem jcbmiGenerateNew;

    MyMenu(JFrame frame){
        JMenuBar menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);

        JMenu mnFile = new JMenu("File");
        menuBar.add(mnFile);

        JMenu mnEnvironment = new JMenu("Environment");
        menuBar.add(mnEnvironment);

        JRadioButtonMenuItem rdbtnmntmReweorderserverint = new JRadioButtonMenuItem("rewe-order-server (INT)");
        rdbtnmntmReweorderserverint.addActionListener(e -> work.setEnvironment("preint"));
        ButtonGroup buttonGroupEnv = new ButtonGroup();
        buttonGroupEnv.add(rdbtnmntmReweorderserverint);
        mnEnvironment.add(rdbtnmntmReweorderserverint);

        rdbtnmntmReweorderservicepre = new JRadioButtonMenuItem("rewe-order-service (PRE)");
        rdbtnmntmReweorderservicepre.addActionListener(arg0 -> work.setEnvironment("prepre"));
        rdbtnmntmReweorderservicepre.setSelected(true);
        buttonGroupEnv.add(rdbtnmntmReweorderservicepre);
        mnEnvironment.add(rdbtnmntmReweorderservicepre);

        rdbtnmntmReweorderservicesitpre = new JRadioButtonMenuItem("rewe-order-service-sit (PRE)");
        rdbtnmntmReweorderservicesitpre.addActionListener(e -> work.setEnvironment("sitpre"));
        buttonGroupEnv.add(rdbtnmntmReweorderservicesitpre);
        mnEnvironment.add(rdbtnmntmReweorderservicesitpre);

        JMenu mnESettings = new JMenu("Settings");
        menuBar.add(mnESettings);

        jcbmiGenerateNew = new JCheckBoxMenuItem("Generate always new Token");
        mnESettings.add(jcbmiGenerateNew);
        jcbmiGenerateNew.setSelected(true);

    }

}
